# Grading Policy for this course

## Assignment 1 (10 Marks)

1. Resume (5 Marks)
    * Table/Transcript - 2
    * Photo - 1
    * Infomation - 1
    * Overall typesetting - 1

2. Article (4 Marks)
    * Math used - 2
    * Bibtex - 1
    * Theorem/Lemma - 1

3. Using Git (1 Mark)

## Assignment 2 (10 Marks)

1. Targets (8 Marks)
    * dvi - 2
    * pdf - 2
    * all - 2
    * clean - 2

2. Bibtex (1 Mark)

3. Using Git (1 Mark)

## Assignment 3 (10 Marks)

1. Name and Address (1 Mark)
2. Image (1.5 Marks)
3. Favicon (1.5 Marks)
4. Link to Resume (1.5 Marks)
5. Relative Addressing (1.5 Marks)
6. Hosting on Bitbucket (2 Marks)
7. Using Git (1 Mark)

## Assignment 4 (10 Marks)

1. Displaying Clock (3 Mark)
    * Deduct 1.5 marks if clock is not running in infinite loop.
    * Deduct 0.5 marks if 'clear' command is not used for proper display.
    * Deduct 0.5 marks if the format is incorrect.(ie. It should be like HH:MM:SS)
    
2. Awk Script (6 Marks)
    * Deduct 3 marks if code is not generic to any number of rows and columns.
    * Deduct 0.5 marks for each mistakes like wrong/missing computation of Total, Min, Max, Mean or Standard Deviation.
    * Do not cut any marks if output is not properly indented.
    
3. Using Git (1 Mark)

## Assignment 5 (10 Marks)

1. Probability density functions (4 marks)
    * Deduct 0.5 mark each if legend, title or axis labels are absent 
    * Deduct 1 mark if np, λ, and μ are not labeled in the graph
    * Deduct 1 mark if the 3 graphs are separate and not in the same plot 
    
2. Generating samples and frequency histograms  (5 marks)
    * Deduct 0.5 mark each if legend, title or axis labels are absent
    * Deduct 1 mark if generated sample files are missing

3. Using Git (1 Mark)
    
## Assignment 6 (10 Marks)

1. Probability density functions (3 marks)
    * Deduct 0.5 mark each if legend, title or axis labels are absent 
    * Deduct 0.5 mark if coloring not done properly as mentioned
    
2. Flowchart (3 marks)
    * Deduct 0.5 mark if there is any layout issues
    * Deduct 0.5 mark if there is any conceptual mistake ie. decision making step not written inside diamond box and so on

3. Kuratowski and Petersen's graphs (3 marks)
    * 1 mark for each of the 3 graphs
    * Students may use different styling, so graph may not look appropriate. But if connectivity between the nodes is correct, do not cut marks.
    * Deduct 0.5 mark if coloring of node is not done properly as mentioned

4. Using Git (1 Mark)


# End semester exam grading Policy

## Summary

* Total marks: 50
* Question 1 : 30
* Question 2 : 20

## Question 1

Octave code problems: if the algorithm is correct but due to minor
syntax issues there is problem please reduce 1 marks.  For this the TA
has to figure out what is mistake and check if the corrected version works
as indented.

1. Generating of samples (Max 5 marks). Few students have generated
   the points only in the first quadrant (i.e. points with x and y
   coordinates in the range (0,1)). Reduce 1 mark for that.

2. Calculating the frequency (Max 5 marks): For checking whether the
   point is inside the circle they can either use the strict
   inequality x² + y² < 1 (i.e. open circle) or x² + y² ≤ 2. Both are
   *fine*.

3. Bin-ing and calculating the Histograms (Max 5). Few have just ploted on x axis the
   sample index and the frequency obtained. This is gives them only 2 marks.
   They should have counted how many samples  S_i have frequence within the bin.
   The final histogram will then tend towards the Probability density function.

4. Final calculation of the value of π. 3 marks

### LaTeX report.

1. Author/title: 1 mark

2. Description of experiment: 1 mark

3. Formula for the calculation of π or equivalent: 2 marks

4. Tikz figure for the circle in square: 2 marks
   
5. Including the Histogram: 1 mark

6. Reporting the value of π: 1 mark

Git repository: 4 marks.


## Question 2

1. Using log to get the relations correctly: 8 marks
    -2 marks if the did not get enough out to generate the graph i.e print only parent hash

2. Generating the dot file correctly: 8 marks

	* -4 marks if merges are not handled correctly; i.e. multiple parents for a single child or vice-versa

3. Getting the labels correct: 2 marks


Git repository 2 
