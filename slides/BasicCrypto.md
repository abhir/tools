% Basic Cryptography
% Piyush P Kurur
% March 9, 2015

# Cryptography

## What is Cryptography?

- Sending secret message

- Authenticating messages


## Secret key Cryptography

- Both parties need to share a secret

- Usually fast

- Unfortunately not very easy to setup.


## Public key methods

- A public key known to every one.

- A secret key known only to the you.

- Encryption can be done using the public key

- Decryption needs the private key.


## Known Public key algorithms

- RSA

- Elliptic curve based ones.


## Digital signature.

- Easy to verify the signature

- Impossible to generate S without secret key

# Integrity checking

## Problem

- A large database D, or a collection of files

- How can I check if the data is not modified

## Cryptographic hashes

- Given a string $X$, $h(X)$ is a small string

- Given $h(X)$ impossible to find $Y$ such that $h(Y) = h(X)$

- Impossible to find  $X ≠ Y$ such that $h(X) = h(Y)$.

## For integrity checking

- You can compute the hash

- Digitally sign it and send it.


# Gnu Privacy Guard

## Concepts

- Master keys

- Sub keys

- Fingerprints

- Revocation

## Exporting and importing

- To send your key you can export it

- You can import the public key of others
