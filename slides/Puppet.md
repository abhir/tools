% Configuration management with puppet
% Piyush P Kurur
% March 9, 2015

# Centralised configuration management.

## Problem

- Manage a large collection of machines.

- Manage a class of machines all having similar configurations

## Puppet

- A declarative language

- A puppet master

- Puppet agents


## Resources

~~~ { .puppet }

file { 'sshdconfig'
   require => Package[ssh-server]
   source  =>

~~~
